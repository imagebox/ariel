(function ($) {
  'use strict';

  /**
   * Site javascripts
   * ===
   */

  // Wrap video embeds in Flexible Container
  $('iframe[src*="youtube.com"]:not(.not-responsive), iframe[src*="vimeo.com"]:not(.not-responsive)').wrap('<div class="flexible-container"></div>');

  $("#tab-1").addClass('current');
  $('ul.tabs li').first().addClass('current');

  $('ul.tabs li').click(function(){

    var tab_id = $(this).attr('data-tab');

    // $('ul.tabs li:first-child').addClass('current');
    $('ul.tabs li').removeClass('current');
    $('.tab-content').removeClass('current');

    $(this).addClass('current');
    $('#'+tab_id).addClass('current');
  });


  $('.open-post').magnificPopup({type: 'inline'});
  // homepage new slide

  $('.video-button').magnificPopup({type: 'iframe'});
  // homepage video link

})(jQuery);



// var listItem = document.querySelectorAll('.list-item img');
// var expandImg = document.querySelector('.expandedImg img');
// var expandCopy = document.querySelector('.expandedImg .imgText');
//
// function galleryImg(imgs){
// 	//var imgText = document.querySelector('.imgText');
// 	expandImg.src = imgs.src;
// 	expandImg.parentElement.style.display='block';
//
// }
//
// function galleryCopy(copy){
//   //expandCopy.src = copy.src;
//   expandCopy.parentElement.style.display='block';
// }
//
// listItem.forEach(function(item, index){
// 	// expandImg.src = itemindex.src;
// 	// if(expandImg.src!==''){
// 	// 	expandImg.src = item.src;
// 	// }
// 	console.log(item.src, index)
// 	item.addEventListener('click', function(){
// 		galleryImg(this);
//     galleryCopy(this);
// 	});


  // tab gallery










  var accordions = document.getElementsByClassName('accordion');



    for (var i = 0; i < accordions.length; i++){
      accordions[i].onclick = function () {
        this.classList.toggle('accord-is-open');
        var content = this.nextElementSibling;

        if (content.style.maxHeight) {
            content.style.maxHeight = null;
        } else {
          content.style.maxHeight = content.scrollHeight + "px";
        }
      };
    }



// end accordions //



//https://www.w3schools.com/howto/howto_js_tab_img_gallery.asp
