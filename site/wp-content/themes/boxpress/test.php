






<?php if ( $tab_video_link ) : ?>
	 <a class="video-button" href="<?php echo esc_url($tab_video_link); ?>">
		 <img draggable="false" aria-hidden="true"
			 src="<?php echo esc_url( $tab_image['sizes'][ $tab_full_image_size ] ); ?>"
			 width="<?php echo esc_attr( $tab_image['sizes'][ $tab_full_image_size . '-width'] ); ?>"
			 height="<?php echo esc_attr( $tab_image['sizes'][ $tab_full_image_size . '-height'] ); ?>"
			 alt="<?php echo esc_attr( $tab_image['alt'] ); ?>">
	 </a>
 <?php else : ?>
		 <a href="<?php echo esc_url($tab_link['url']); ?>"
			 target="<?php echo esc_url($tab_link['target']); ?>">
			 <img draggable="false" aria-hidden="true"
				 src="<?php echo esc_url( $tab_image['sizes'][ $tab_full_image_size ] ); ?>"
				 width="<?php echo esc_attr( $tab_image['sizes'][ $tab_full_image_size . '-width'] ); ?>"
				 height="<?php echo esc_attr( $tab_image['sizes'][ $tab_full_image_size . '-height'] ); ?>"
				 alt="<?php echo esc_attr( $tab_image['alt'] ); ?>">
		 </a>
 <?php endif; ?>



 <?php if ( $tab_video_link ) : ?>
 	 <a class="video-button" href="<?php echo esc_url($tab_video_link); ?>">
 		 <img draggable="false" aria-hidden="true"
 			 src="<?php echo esc_url( $tab_image['sizes'][ $tab_full_image_size ] ); ?>"
 			 width="<?php echo esc_attr( $tab_image['sizes'][ $tab_full_image_size . '-width'] ); ?>"
 			 height="<?php echo esc_attr( $tab_image['sizes'][ $tab_full_image_size . '-height'] ); ?>"
 			 alt="<?php echo esc_attr( $tab_image['alt'] ); ?>">
 	 </a>
  <?php else : ?>
 		 <a href="<?php echo esc_url($tab_link['url']); ?>"
 			 target="<?php echo esc_url($tab_link['target']); ?>">
 			 <img draggable="false" aria-hidden="true"
 				 src="<?php echo esc_url( $tab_image['sizes'][ $tab_full_image_size ] ); ?>"
 				 width="<?php echo esc_attr( $tab_image['sizes'][ $tab_full_image_size . '-width'] ); ?>"
 				 height="<?php echo esc_attr( $tab_image['sizes'][ $tab_full_image_size . '-height'] ); ?>"
 				 alt="<?php echo esc_attr( $tab_image['alt'] ); ?>">
 		 </a>
  <?php endif; ?>
