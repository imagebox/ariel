<?php
/**
 * Displays the staff member template
 */

   $people_image  = get_field( 'people_image' );
   $people_copy  = get_field( 'people_copy' );
   $name_link = get_field( 'name_link' );
   $people_headshot = 'headshot_images';
?>

<div class="staff-box">
  <div class="box-border">
    <?php
    if( $name_link ) { ?>
      <a
        href="<?php echo esc_url( $name_link['url'] ); ?>"
        target="<?php echo esc_attr( $name_link['target'] ); ?>">
        <div class="staff-image">
          <img draggable="false" aria-hidden="true"
            src="<?php echo esc_url( $people_image['sizes'][ $people_headshot ] ); ?>"
            width="<?php echo esc_attr( $people_image['sizes'][ $people_headshot . '-width'] ); ?>"
            height="<?php echo esc_attr( $people_image['sizes'][ $people_headshot . '-height'] ); ?>"
            alt="<?php echo esc_attr( $people_image['alt'] ); ?>">
        </div>
      </a>

      <?php
    } else { ?>   <div class="staff-image staff-padding">
        <img draggable="false" aria-hidden="true"
          src="<?php echo esc_url( $people_image['sizes'][ $people_headshot ] ); ?>"
          width="<?php echo esc_attr( $people_image['sizes'][ $people_headshot . '-width'] ); ?>"
          height="<?php echo esc_attr( $people_image['sizes'][ $people_headshot . '-height'] ); ?>"
          alt="<?php echo esc_attr( $people_image['alt'] ); ?>">
      </div>
         <?php }
       ?>

  </a>
  </div>
  <div class="staff-text">
    <?php
    if( $name_link ) { ?>
      <a
        href="<?php echo esc_url( $name_link['url'] ); ?>"
        target="<?php echo esc_attr( $name_link['target'] ); ?>">
        <h3>
          <?php the_title(); ?>
        </h3>
      </a>

      <?php
    } else { ?>  <h3>
          <?php the_title(); ?>
        </h3> <?php }
       ?>
    <p><?php echo $people_copy; ?></p>
  </div>
</div>
