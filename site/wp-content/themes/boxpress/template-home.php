<?php
/**
 * Template Name: Homepage
 *
 * Page template to display the homepage.
 *
 * @package boxpress
 */
get_header(); ?>

<?php


 ?>

  <article class="homepage">

    <?php // Hero ?>
    <?php get_template_part( 'template-parts/homepage/homepage-hero' ); ?>


    <section class="highlight-section section">
      <div class="wrap">

        <?php if ( have_rows( 'highlight_row' ) ) : ?>
          <?php while ( have_rows( 'highlight_row' ) ) : the_row();

          $highlight_heading_section = get_sub_field('highlight_heading_section');
          $highlight_subheading_section = get_sub_field('highlight_subheading_section');

          ?>

          <div class="highlight-heading section">
            <h2><?php echo $highlight_heading_section; ?></h2>
            <p><?php echo $highlight_subheading_section; ?></p>
          </div>

          <div class="l-grid l-grid--two-col">
          <?php if ( have_rows( 'highlight_row_column' ) ) : ?>
            <?php $counter_1 = 1; ?>
            <?php while ( have_rows( 'highlight_row_column' ) ) : the_row();

            $highlight_heading = get_sub_field('highlight_heading');
            $highlight_description = get_sub_field('highlight_description');
            $highlight_link = get_sub_field('highlight_link');
            $highlight_photo = get_sub_field('highlight_photo');
            $highlight_photo_size = 'highlight_photo_size';

            ?>

          <div class="l-grid-item">
            <a
              href="<?php echo esc_url( $highlight_link['url'] ); ?>"
              target="<?php echo esc_attr( $highlight_link['target'] ); ?>">
            <div class="box">
              <div class="box-container">
                <div class="number">
                  <?php echo $counter_1; ?>
                </div>
                <div class="line"></div>
                <div class="graphic">
                  <img draggable="false" aria-hidden="true"
                  src="<?php echo esc_url( $highlight_photo['sizes'][ $highlight_photo_size ] ); ?>"
                  width="<?php echo esc_attr( $highlight_photo['sizes'][ $highlight_photo_size . '-width'] ); ?>"
                  height="<?php echo esc_attr( $highlight_photo['sizes'][ $highlight_photo_size . '-height'] ); ?>"
                  alt="<?php echo esc_attr( $highlight_photo['alt'] ); ?>">
                </div>
              </div>
              <div class="box-content">
                <h5><?php echo $highlight_heading; ?></h5>
                <?php if ( $highlight_description ) : ?>
                    <p><?php echo $highlight_description; ?></p>
                <?php endif; ?>
                <?php if ( $highlight_link ) : ?>
                  <a class="text-button"
                    href="<?php echo esc_url( $highlight_link['url'] ); ?>"
                    target="<?php echo esc_attr( $highlight_link['target'] ); ?>">
                    <?php echo $highlight_link['title']; ?>
                  </a>
                <?php endif; ?>
              </div>
            </div>
           </a>
          </div>
          <?php $counter_1++; // add one per row ?>
            <?php endwhile; ?>
          <?php endif; ?>
          <?php endwhile; ?>
        <?php endif; ?>
      </div>
      </div>
    </section>
    <!-- Recent Talks -->

<!-- recent talk -->

<section class="section recent-talks">
  <?php

  $recent_talk_heading = get_field('recent_talk_heading');
  $recent_talk_description = get_field('recent_talk_description');

   ?>
 <div class="wrap">
   <div class="tab-heading">
     <h2><?php echo $recent_talk_heading; ?></h2>
     <?php if ( $recent_talk_description ) : ?>
         <p><?php echo $recent_talk_description; ?></p>
     <?php endif; ?>
   </div>
 </div>
  <div class="wrap">



  	<?php if( have_rows('tab_gallery_row') ): $i = 0; ?>
  		<ul class="tabs">
  		<?php while( have_rows('tab_gallery_row') ): the_row(); $i++;
  			// vars
  			$tab_image = get_sub_field('tab_image');
  			$tab_venue = get_sub_field('tab_venue');
  			$tab_abstract = get_sub_field('tab_abstract');
        $tab_venue_heading = get_sub_field('tab_venue_heading');
        $tab_abstract_heading = get_sub_field('tab_abstract_heading');
  			$tab_image_size = 'tab_image_size';
        $tab_video_link = get_sub_field('tab_video_link');
        $tab_link = get_sub_field('tab_link');
        $tab_image_size = ('tab_image_size');
  			?>

  			<li class="tab-link" data-tab="tab-<?php echo $i; ?>">
          <img draggable="false" aria-hidden="true"
            src="<?php echo esc_url( $tab_image['sizes'][ $tab_image_size ] ); ?>"
            width="<?php echo esc_attr( $tab_image['sizes'][ $tab_image_size . '-width'] ); ?>"
            height="<?php echo esc_attr( $tab_image['sizes'][ $tab_image_size . '-height'] ); ?>"
            alt="<?php echo esc_attr( $tab_image['alt'] ); ?>">
  	    </li>
  		<?php endwhile; ?>
  		</ul>
  	<?php endif; ?>


    	<?php if( have_rows('tab_gallery_row') ): $i = 0; ?>
        <div class="tabbed-content-box">
    		<?php while( have_rows('tab_gallery_row') ): the_row(); $i++;
    			// vars
          $tab_image = get_sub_field('tab_image');
          $tab_venue = get_sub_field('tab_venue');
          $tab_abstract = get_sub_field('tab_abstract');
          $tab_venue_heading = get_sub_field('tab_venue_heading');
          $tab_abstract_heading = get_sub_field('tab_abstract_heading');
    			$tab_image_size = 'tab_image_size';
          $tab_video_link = get_sub_field('tab_video_link');
          $tab_link = get_sub_field('tab_link');
          $tab_full_image_size = ('tab_full_image_size');

  			?>
    			<div id="tab-<?php echo $i; ?>" class="tab-content">
           <div class="tab-full-image">
             <?php if ( $tab_video_link ) : ?>
             	 <a class="video-button" href="<?php echo esc_url($tab_video_link); ?>">
             		 <img draggable="false" aria-hidden="true"
             			 src="<?php echo esc_url( $tab_image['sizes'][ $tab_full_image_size ] ); ?>"
             			 width="<?php echo esc_attr( $tab_image['sizes'][ $tab_full_image_size . '-width'] ); ?>"
             			 height="<?php echo esc_attr( $tab_image['sizes'][ $tab_full_image_size . '-height'] ); ?>"
             			 alt="<?php echo esc_attr( $tab_image['alt'] ); ?>">
                   <svg width="67px" height="67px" viewBox="0 0 67 67" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                       <!-- Generator: Sketch 58 (84663) - https://sketch.com -->
                       <title>Group 13</title>
                       <desc>Created with Sketch.</desc>
                       <g id="Final" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                           <g id="Home-V2" transform="translate(-793.000000, -2196.000000)" stroke="#454149" stroke-width="4">
                               <g id="Group-14-Copy" transform="translate(485.000000, 2028.000000)">
                                   <g id="Group-13" transform="translate(308.000000, 168.000000)">
                                       <circle id="Oval" fill="#6E6974" cx="33.5" cy="33.5" r="31.5"></circle>
                                       <path d="M37,25.9692697 L26.482074,44 L47.517926,44 L37,25.9692697 Z" id="Triangle-Copy-14" fill="#FFFFFF" transform="translate(37.000000, 34.000000) rotate(-270.000000) translate(-37.000000, -34.000000) "></path>
                                   </g>
                               </g>
                           </g>
                       </g>
                   </svg>
             	 </a>
              <?php else : ?>
             		 <a href="<?php echo esc_url($tab_link['url']); ?>"
             			 target="<?php echo esc_url($tab_link['target']); ?>">
             			 <img draggable="false" aria-hidden="true"
             				 src="<?php echo esc_url( $tab_image['sizes'][ $tab_full_image_size ] ); ?>"
             				 width="<?php echo esc_attr( $tab_image['sizes'][ $tab_full_image_size . '-width'] ); ?>"
             				 height="<?php echo esc_attr( $tab_image['sizes'][ $tab_full_image_size . '-height'] ); ?>"
             				 alt="<?php echo esc_attr( $tab_image['alt'] ); ?>">
             		 </a>
              <?php endif; ?>
           </div>
          <div class="tab-description">
            <div class="tab-venue">
              <?php if ( $tab_venue ) : ?>
                <h5><?php echo $tab_venue_heading; ?></h5>
                  <p><?php echo $tab_venue ?></p>
              <?php endif; ?>
            </div>
            <div class="tab-abstract">
              <?php if ( $tab_abstract ) : ?>
                <h5><?php echo $tab_abstract_heading; ?></h5>
                  <p><?php echo $tab_abstract ?></p>
              <?php endif; ?>
            </div>
          </div>
    			</div>
    		<?php endwhile; ?>
  		</div>
  	<?php endif; ?>

  </div><!-- container -->
</section>


<section class="section news-slide-section">




  <?php if ( have_rows ('news_slide') ) : ?>
    <?php while( have_rows ('news_slide') ) : the_row();

    $news_heading = get_sub_field('news_heading');
    $news_subheading = get_sub_field('news_subheading');

    ?>

    <div class="wrap">
      <div class="header open-post">
        <h2><?php echo $news_heading; ?></h2>
          <div class="slider-controls-box">
            <a id="news-previous" class="prev-left" href="#">
              <svg width="28px" height="18px" viewBox="0 0 34 26" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                  <!-- Generator: Sketch 58 (84663) - https://sketch.com -->
                  <g id="Final" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                      <g id="Home-V2" transform="translate(-989.000000, -2800.000000)" stroke="#454149" stroke-width="4">
                          <g id="Group-17" transform="translate(992.000000, 2797.000000)">
                              <g id="Group-18" transform="translate(-0.000000, 5.000000)">
                                  <polyline id="Path-6" points="12.0719781 -2.23820962e-13 4.54747351e-13 10.4873557 12.0719781 21.8932029"></polyline>
                                  <path d="M-1.77635684e-15,10.9466014 L30.4293217,10.9466014" id="Path-7"></path>
                              </g>
                          </g>
                      </g>
                  </g>
              </svg>
            </a>
            <a id="news-next" class="next-right" href="#">
              <svg width="28px" height="18px" viewBox="0 0 29 22" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                  <!-- Generator: Sketch 58 (84663) - https://sketch.com -->
                  <g id="Final" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                      <g id="Home-V2" transform="translate(-1156.000000, -2800.000000)" stroke="#454149" stroke-width="4">
                          <g id="Group-17" transform="translate(995.000000, 2797.000000)">
                              <g id="Group-16" transform="translate(161.000000, 5.000000)">
                                  <g id="Group-19" transform="translate(-0.000000, -0.000000)">
                                      <polyline id="Path-6-Copy" transform="translate(20.159217, 9.181021) rotate(-180.000000) translate(-20.159217, -9.181021) " points="25.221659 -2.23820962e-13 15.0967742 8.79584669 25.221659 18.3620411"></polyline>
                                      <path d="M-1.77635684e-15,9.18102056 L25.5213666,9.18102056" id="Path-7-Copy"></path>
                                  </g>
                              </g>
                          </g>
                      </g>
                  </g>
              </svg>
            </a>
          </div>
        <?php if ( $news_subheading ) : ?>
            <p><?php echo $news_subheading ?></p>
        <?php endif; ?>
      </div>
    </div>

<div class="wrap fade-graphic">

    <div class="js-carousel">

          <?php
          $home_post_query_args = array(
            'post_type' => 'post',
             'posts_per_page' => -1,
             'orderby' => 'date',
             'order' => 'DESC',
          );
          $home_post_query = new WP_Query( $home_post_query_args );

           ?>

          <?php if ( $home_post_query->have_posts() ) : ?>
             <?php while ( $home_post_query->have_posts() ) : $home_post_query->the_post(); ?>
          <div class="news-slide">
            <a class="open-post" href="#popup-<?php echo get_the_ID(); ?>">
              <div class="box">
                <div class="box-box">
                  <svg preserveAspectRatio="none" width="145px" height="89px" viewBox="0 0 145 89" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                      <!-- Generator: Sketch 58 (84663) - https://sketch.com -->
                      <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                          <g id="Group" fill="#454149" fill-rule="nonzero">
                              <polygon id="Line-3-Copy-3" points="60.2951021 1.01370631 22.6173338 75 18.1784668 75 55.81143 1.00719744"></polygon>
                              <polygon id="Line-3-Copy-6" points="128.336044 1.00517751 90.6690826 75 86.1777649 75 123.819087 0.99209293"></polygon>
                              <polygon id="Line-3-Copy" points="14.3055115 1.00517751 0.0365663081 29.0638324 0.00363393879 20.2967672 9.81464638 1.00160492"></polygon>
                              <polygon id="Line-3-Copy-4" points="82.2990702 1.00517751 44.6668854 75 40.1701355 75 77.8125347 1.00442788"></polygon>
                              <polygon id="Line-3-Copy-7" points="147.051191 9.36111447 113.673782 75 109.193863 75 146.813019 1.00517751 147 1.00517751"></polygon>
                              <polygon id="Path" points="37.3062134 1.01378851 7.10952596e-15 74.3662415 7.10542736e-15 65.5388794 32.8084775 1.01378851"></polygon>
                              <polygon id="Line-3-Copy-5" points="105.296478 1.00517751 67.6763 75 63.1841125 75 100.819427 1"></polygon>
                              <polygon id="Line-3-Copy-8" points="147.002388 54.6913888 136.672239 75.00753 132.183444 75 147.002388 45.8750153"></polygon>
                          </g>
                      </g>
                  </svg>
                </div>
                <div class="box-thumb">
                  <?php the_post_thumbnail('news_slider_thumb');?>
                </div>

                <div class="box-body">
                  <h6><?php the_date('M j, Y'); ?></h6>
                  <h3><?php the_title(); ?></h3>
                  <a class="text-button"><?php _e('Expand', 'boxpress'); ?></a>
                  <a class="trickery-link open-post" href="#popup-<?php echo get_the_ID(); ?>"></a>
                </div>

              </div>
            </a>

            <div id="popup-<?php echo get_the_ID(); ?>" class="mfp-hide popup">
              <div class="wrap modal-wrap">
                <div class="modal-content">
                  <div class="modal-header">
                    <button title="Close (Esc)" type="button" class="mfp-close">  <span class="vh"><?php _e('Close', 'boxpress'); ?></span>
                      <svg class="menu-icon-svg" width="33" height="33" focusable="false">
                        <use href="#close-icon"/>
                      </svg><span class="custom-close"></span></button>
                 <h2><?php the_title(); ?></h2>
                 <?php the_content(); ?>
                  </div>

                </div>
              </div>
            </div>
          </div>



        <?php endwhile; ?>
      <?php endif; ?>

      <!-- nested repeater row -->
  <!-- blog query -->
    </div>

    <?php endwhile; ?>
  <?php endif; ?>
  <!-- row repeater row -->
</div>
</div>
</section>
<!-- Slider -->

 <?php wp_reset_postdata(); ?>

</article>

<?php get_footer(); ?>
