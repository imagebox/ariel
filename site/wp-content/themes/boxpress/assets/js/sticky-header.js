// (function ($) {
//   'use strict';
//
//   /**
//    * Sticky Header
//    * ===
//    */
//
//   var $site_header = $('#masthead');
//
//   // Init Sticky Nav
//   toggleStickyNav( $site_header );
//
//   // Re-calculate sticky on scroll
//   $(window).on( 'scroll', function () {
//     toggleStickyNav( $site_header );
//   });
//
//   // Re-calculate sticky on window resize
//   $(window).on( 'resize', function () {
//     toggleStickyNav( $site_header );
//   });
//
//   function toggleStickyNav() {
//     var $site_header = $('#masthead');
//
//     if ( $site_header.is( ':visible' ) &&
//          $(window).scrollTop() > 40 ) {
//       $site_header.addClass('minify-header');
//     } else {
//       $site_header.removeClass('minify-header');
//     }
//   }
//
// })(jQuery);
