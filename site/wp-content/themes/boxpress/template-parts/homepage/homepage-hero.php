<?php
/**
 * Displays the Hero layout
 *
 * Easily convertible into a sideshow by enabling multiple
 * rows in the repeater.
 *
 * @package boxpress
 */
?>
<?php if ( have_rows( 'homepage_hero' )) : ?>
  <?php while ( have_rows( 'homepage_hero' )) : the_row();
      $hero_heading     = get_sub_field( 'hero_heading' );
      $hero_title     = get_sub_field( 'hero_title' );
      $hero_subheading  = get_sub_field( 'hero_subheading' );
      $hero_link        = get_sub_field( 'hero_link' );
      $hero_link        = get_sub_field( 'hero_link' );
      $hero_photo        = get_sub_field( 'hero_photo' );
      $hero_photo_size        = 'hero_photo_size' ;
      $icon_1 = get_sub_field('icon_1');
      $icon_2 = get_sub_field('icon_2');
      $icon_3 = get_sub_field('icon_3');
      $icon_heading_1 = get_sub_field('icon_heading_1');
      $icon_heading_2 = get_sub_field('icon_heading_2');
      $icon_heading_3 = get_sub_field('icon_heading_3');
      $icon_text_1 = get_sub_field('icon_text_1');
      $icon_text_2 = get_sub_field('icon_text_2');
      $icon_text_3 = get_sub_field('icon_text_3');
    ?>

    <section class="hero">
      <div class="wrap hero">
        <div class="hero-box">
          <div class="hero-content">
            <h1><?php echo $hero_heading; ?></h1>
            <h6><?php echo $hero_title; ?></h6>

            <?php if ( ! empty( $hero_subheading )) : ?>
              <?php echo $hero_subheading; ?>
            <?php endif; ?>

            <!-- <div class="hero-icons">
              <div class="icon-row">
                <img src="<?php echo $icon_1; ?>" alt="">
                <h6><?php echo $icon_heading_1; ?></h6>
                <p><?php echo $icon_text_1; ?></p>
              </div>
              <div class="icon-row">
                <img src="<?php echo $icon_2; ?>" alt="">
                <h6><?php echo $icon_heading_2; ?></h6>
                <p><?php echo $icon_text_2; ?></p>
              </div>
              <div class="icon-row">
                <img src="<?php echo $icon_3; ?>" alt="">
                <h6><?php echo $icon_heading_3; ?></h6>
                <p><?php echo $icon_text_3; ?></p>
              </div>
            </div> -->
          </div>

          <!-- hero sub copy -->

        </div>
        <div class="photo-box-container">
          <div class="photo-box">
            <img draggable="false" aria-hidden="true"
              src="<?php echo esc_url( $hero_photo['sizes'][ $hero_photo_size ] ); ?>"
              width="<?php echo esc_attr( $hero_photo['sizes'][ $hero_photo_size . '-width'] ); ?>"
              height="<?php echo esc_attr( $hero_photo['sizes'][ $hero_photo_size . '-height'] ); ?>"
              alt="<?php echo esc_attr( $hero_photo['alt'] ); ?>">

          </div>
          <div class="inner-box"></div>
      </div>
    </div>
    </section>

  <?php endwhile; ?>
<?php endif; ?>
