<?php
/**
 * Template Name: Teaching
 *
 * Page template to display the advanced page builder.
 *
 * @package BoxPress
 */
get_header(); ?>

  <?php require_once('template-parts/banners/banner--page.php'); ?>


      <?php if ( have_rows( 'general_row' ) ) : ?>
        <?php while ( have_rows( 'general_row' ) ) : the_row();


        $teaching_title = get_sub_field('teaching_title');
        $teaching_code = get_sub_field('teaching_code');
        $teaching_location = get_sub_field('teaching_location');
        $course_description = get_sub_field('course_description');
        $teaching_date_name = get_sub_field('teaching_date_name');
        $teaching_color = get_sub_field('teaching_color');
        $teaching_link = get_sub_field('teaching_link');

        ?>

      <section class="section teaching-section">
        <div class="wrap">
          <div class="teaching-grid">
            <div class="teaching-item">
              <div class="teaching-card">
                <a href="<?php echo esc_url( $teaching_link['url'] ); ?>"
                    target="<?php echo esc_attr( $teaching_link['target'] ); ?>">
                  <div class="teaching-card-header">
                    <h3><?php echo $teaching_title; ?></h3>
                  </div>
                  <div class="teaching-card-footer">
                     <div class="card-item <?php echo $teaching_color; ?>">
                       <h4 class=""><?php echo $teaching_code; ?></h4>
                     </div>
                     <div class="card-item">

                     </div>
                 </div>
                </a>
              </div>
           </div>
           <div class="teaching-item scrollbar">
             <div class="location-info">
               <?php if ( ! empty( $teaching_location )) : ?>
                 <h5>School</h5>
                 <p><?php echo $teaching_location; ?></p>
               <?php endif; ?>
             </div>
             <div class="course-info">
               <?php if ( ! empty( $course_description )) : ?>
                 <h5>Course Description</h5>
                 <p><?php echo $course_description; ?></p>
               <?php endif; ?>
             </div>
             <div class="details-info">

               <?php
                $details_title = get_sub_field('details_title');
                ?>
               <?php if ( ! empty( $details_title )) : ?>
                 <h5><?php echo $details_title ?></h5>
               <?php endif; ?>
               <ul>
               <?php if ( have_rows( 'details_row' ) ) :   ?>


                 <?php while ( have_rows( 'details_row' ) ) : the_row();
                 $course_details = get_sub_field('course_details');
                 ?>

                 <?php if ( ! empty( $course_details )) : ?>
                   <li>
                   <?php echo $course_details; ?>
                   </li>
                 <?php endif; ?>
               <?php endwhile; ?>
             <?php endif; ?>
           </ul>
           </div>
           </div>
            <div class="teaching-item-last scrollbar">
              <h5>Semesters</h5>
              <div class="date-name">
                 <div class="date-name-item">
                   <?php if ( have_rows( 'month_year_row' ) ) : ?>
                     <?php while ( have_rows( 'month_year_row' ) ) : the_row();
                     $date = get_sub_field('date');
                     $date_or_date_link  = get_sub_field( 'date_or_date_link' );
                     $date_link = get_sub_field('date_link');
                     $instructor_name = get_sub_field('instructor_name');
                     $instructor_name_or_instructor_link  = get_sub_field( 'instructor_name_or_instructor_link' );
                     $instructor_link = get_sub_field('instructor_link');
                     ?>

                     <?php
                     if( get_sub_field('date_or_date_link') == 'date' ) { ?>

                     <?php if ( ! empty( $date )) : ?>
                      <h5> <?php echo $date; ?></h5>
                     <?php endif; ?>

                       <?php
                     } else { ?>

                    <?php if ( ! empty( $date_link )) : ?>
                    <h5>   <a
                                            href="<?php echo esc_url( $date_link['url'] ); ?>"
                                            target="<?php echo esc_attr( $date_link['target'] ); ?>">
                                           <?php echo $date_link['title']; ?>
                                          </a></h5>
                      <?php endif; ?>

                      <?php }
                        ?>



                     <?php
                     if( get_sub_field('instructor_name_or_instructor_link') == 'instructor_name' ) { ?>

                     <?php if ( ! empty( $instructor_name )) : ?>
                       <p>With <?php echo $instructor_name; ?></p>
                     <?php endif; ?>

                       <?php
                     } else { ?>

                    <?php if ( ! empty( $instructor_link )) : ?>
                       With <a
                          href="<?php echo esc_url( $instructor_link['url'] ); ?>"
                          target="<?php echo esc_attr( $instructor_link['target'] ); ?>">
                         <?php echo $instructor_link['title']; ?>
                        </a>
                      <?php endif; ?>

                      <?php }
                        ?>
                   <?php endwhile; ?>
                 <?php endif; ?>
                 </div>
              </div>
            </div>
          </div>
        </div>
      </section>

        <?php endwhile; ?>
      <?php endif; ?>
      <!-- col -->

<!-- tab -->


<?php get_footer(); ?>
