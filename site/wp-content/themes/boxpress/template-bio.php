<?php
/**
 * Template Name: Bio
 *
 * Page template to display the advanced page builder.
 *
 * @package BoxPress
 */
get_header(); ?>

  <?php require_once('template-parts/banners/banner--page.php'); ?>

<section class="section bio-section">
  <div class="wrap">
    <div class="l-grid l-grid--two-col">
      <div class="l-grid-item">
        <h3>Education</h3>
        <?php if ( have_rows( 'education_row' ) ) : ?>
          <?php $counter = 1; ?>
          <?php while ( have_rows( 'education_row' ) ) : the_row();

          $bio_year = get_sub_field('bio_year');
          $bio_copy = get_sub_field('bio_copy');
          $date_color = get_sub_field('date_color');

           ?>

           <div class="content-row">
               <div class="box-container">
                 <div class="box <?php echo $date_color; ?>">
                  <?php echo $counter; ?>
                 </div>
               </div>
               <span class="date"><?php echo $bio_year; ?></span>
               <div class="bio-copy">
                 <?php echo $bio_copy; ?>
               </div>
             </div>
          <?php $counter++; // add one per row ?>
          <?php endwhile; ?>
        <?php endif; ?>



        <h3 class="horizontal-line">Positions</h3>
        <?php if ( have_rows( 'position_row' ) ) : ?>
        <?php $counter = 1; ?>
          <?php while ( have_rows( 'position_row' ) ) : the_row();

          $bio_year = get_sub_field('bio_year');
          $bio_copy = get_sub_field('bio_copy');
          $date_color = get_sub_field('date_color');

           ?>

           <div class="content-row">
               <div class="box-container">
                 <div class="box <?php echo $date_color; ?>">
                  <?php echo $counter; ?>
                 </div>
               </div>
               <span class="date"><?php echo $bio_year; ?></span>
               <div class="bio-copy"><?php echo $bio_copy; ?></div>
             </div>
           <?php $counter++; // add one per row ?>
          <?php endwhile; ?>
        <?php endif; ?>

      </div>


      <div class="l-grid-item">
        <?php $misc_copy_awards = get_field('misc_copy_awards'); ?>
        <h3>Awards</h3>
        <?php if ( have_rows( 'awards_row' ) ) : ?>
        <?php $counter = 1; ?>
          <?php while ( have_rows( 'awards_row' ) ) : the_row();


          $bio_year = get_sub_field('bio_year');
          $bio_copy = get_sub_field('bio_copy');
          $date_color = get_sub_field('date_color');

           ?>

           <div class="content-row">
               <div class="box-container">
                 <div class="box <?php echo $date_color; ?>">
                  <?php echo $counter; ?>
                 </div>
               </div>
               <span class="date"><?php echo $bio_year; ?></span>
               <div class="bio-copy"><?php echo $bio_copy; ?></div>
             </div>
           <?php $counter++; // add one per row ?>


          <?php endwhile; ?>
        <?php endif; ?>

      <?php if ( $misc_copy_awards ) : ?>
        <div class="extra-box">
          <?php echo $misc_copy_awards ?>
        </div>
      <?php endif; ?>
      </div>
    </div>

  </div>
</section>

<section class="section bio-section">
  <div class="wrap">
    <?php $professional_activities_heading = get_field('professional_activities_heading');  ?>

    <div class="heading">
      <h2><?php echo $professional_activities_heading; ?></h2>
    </div>
    <div class="l-grid l-grid--two-col">
      <div class="l-grid-item">
          <h3>Journals</h3>
          <?php if ( have_rows( 'journals_row' ) ) : ?>
          <?php $counter = 1; ?>
            <?php while ( have_rows( 'journals_row' ) ) : the_row();


            $bio_year = get_sub_field('bio_year');
            $bio_copy = get_sub_field('bio_copy');
            $date_color = get_sub_field('date_color');

             ?>

             <div class="content-row">
                 <div class="box-container">
                   <div class="box <?php echo $date_color; ?>">
                    <?php echo $counter; ?>
                   </div>
                 </div>
             <span class="date"><?php echo $bio_year; ?></span>
                 <div class="bio-copy"><?php echo $bio_copy; ?></div>
               </div>
             <?php $counter++; // add one per row ?>


            <?php endwhile; ?>
          <?php endif; ?>
       </div>
       <div class="l-grid-item">
           <?php $misc_copy_conferences = get_field('misc_copy_conferences'); ?>
           <h3>Conference</h3>
           <?php if ( have_rows( 'conference_row' ) ) : ?>
           <?php $counter = 1; ?>
             <?php while ( have_rows( 'conference_row' ) ) : the_row();


             $bio_year = get_sub_field('bio_year');
             $bio_copy = get_sub_field('bio_copy');
             $date_color = get_sub_field('date_color');

              ?>

              <div class="content-row">
                  <div class="box-container">
                    <div class="box <?php echo $date_color; ?>">
                     <?php echo $counter; ?>
                    </div>
                  </div>
                 <span class="date"><?php echo $bio_year; ?></span>
                  <div class="bio-copy"><?php echo $bio_copy; ?></div>
                </div>
              <?php $counter++; // add one per row ?>


             <?php endwhile; ?>
           <?php endif; ?>

           <?php if ( $misc_copy_conferences ) : ?>
             <div class="extra-box">
               <?php echo $misc_copy_conferences ?>
             </div>
           <?php endif; ?>

           <h3 class="horizontal-line">Professional Societies</h3>
           <?php if ( have_rows( 'professional_societies_row' ) ) : ?>
           <?php $counter = 1; ?>
             <?php while ( have_rows( 'professional_societies_row' ) ) : the_row();


             $bio_year = get_sub_field('bio_year');
             $bio_copy = get_sub_field('bio_copy');
             $date_color = get_sub_field('date_color');

              ?>

              <div class="content-row">
                  <div class="box-container">
                    <div class="box <?php echo $date_color; ?>">
                     <?php echo $counter; ?>
                    </div>
                  </div>
                 <span class="date"><?php echo $bio_year; ?></span>
                  <div class="bio-copy"><?php echo $bio_copy; ?></div>
                </div>
              <?php $counter++; // add one per row ?>
             <?php endwhile; ?>
           <?php endif; ?>
        </div>
    </div>
  </div>
</section>



<?php get_footer(); ?>
