<?php
/**
 * Template Name: Publication
 *
 * Page template to display the advanced page builder.
 *
 * @package BoxPress
 */

  $publication_year  = get_query_var( 'publication_year' );
  $publication_type  = get_query_var( 'publication_type' );

get_header(); ?>

  <?php require_once('template-parts/banners/banner--page.php'); ?>

  <?php
    $post_query = array(
      'post_type' => 'publication',
      'posts_per_page' => -1,
      'meta_key' => 'date',
      'orderby' => 'meta_value',
      'order' => 'DESC',
      'tax_query' => array(
        array (
            'taxonomy' => 'publication_type',
            'field' => 'slug',
            'terms' => 'w',
        )
      ),
    );

    // Query for Year
    if ( ! empty( $publication_year )) {
      $post_query = array_merge_recursive( $post_query, array(
        'tax_query' => array(
          array(
            'taxonomy'  => 'publication_year',
            'field'     => 'slug',
            'terms'     => $publication_year,
          ),
        ),
      ));
    }

    // Query for Type
    if ( ! empty( $publication_type )) {
      $post_query = array_merge_recursive( $post_query, array(
        'tax_query' => array(
          array(
            'taxonomy'  => 'publication_type',
            'field'     => 'slug',
            'terms'     => $publication_type,
          ),
        ),
      ));
    }

    $post_loop = new WP_Query( $post_query );
  ?>


  <section class="section form-section">
    <form action="<?php echo esc_url( site_url( '/publications/' )); ?>">
        <div class="wrap">
          <div class="filter-inner">
            <div class="filter-body">
              <div class="filter-item-grid-wrap">
                <div class="filter-item-grid">

                  <?php
                    $publication_type_terms = get_terms( array(
                      'taxonomy'    => 'publication_type',
                      'hide_empty'  => true,
                    ));
                  ?>
                  <?php if ( $publication_type_terms && ! is_wp_error( $publication_type_terms )) : ?>
                    <div class="filter-item">
                      <label for="publication_type"><?php _e('Type', 'boxpress'); ?></label>
                      <div class="select">

                      <select id="publication_type" class="ui-select ui-select--blue" name="publication_type">
                        <option value=""><?php _e( 'All Types', 'boxpress' ); ?></option>

                        <?php foreach ( $publication_type_terms as $term ) : ?>
                          <option value="<?php echo esc_attr( $term->slug ); ?>" <?php
                              if ( $publication_type === $term->slug ) {
                                echo 'selected';
                              }
                            ?>><?php echo $term->name; ?></option>
                        <?php endforeach; ?>

                      </select>
                      <div class="select-arrow"></div>
                    </div>
                    </div>

                  <?php endif; ?>



                  <?php
                    $publication_year_terms = get_terms( array(
                      'taxonomy'    => 'publication_year',
                      'hide_empty'  => true,
                    ));
                  ?>
                  <?php if ( $publication_year_terms && ! is_wp_error( $publication_year_terms )) : ?>
                    <div class="filter-item">
                      <label for="publication_year"><?php _e('Year', 'boxpress'); ?></label>
                      <div class="select">
                      <select id="publication_year" class="ui-select ui-select--blue" name="publication_year">

                      <div class="select-arrow">ddd</div>
                        <option value=""><?php _e( 'All Years', 'boxpress' ); ?></option>

                        <?php foreach ( $publication_year_terms as $term ) : ?>
                          <option value="<?php echo esc_attr( $term->slug ); ?>" <?php
                              if ( $publication_year === $term->slug ) {
                                echo 'selected';
                              }
                            ?>><?php echo $term->name; ?></option>
                        <?php endforeach; ?>

                      </select>
                      <div class="select-arrow"></div>
                    </div>
                    </div>
                  <?php endif; ?>


                </div>
              </div>
              <footer class="filter-footer">
                <div>
                  <button class="button" type="submit"><?php _e('Filter', 'boxpress'); ?></button>
                </div>
              </footer>
            </div>
          </div>
        </div>
      </form>
  </section>


  <?php if ( $post_loop->have_posts() ) : ?>
  <section class="section paper-section first">
    <div class="wrap">
      <header>
        <h2>Working Papers</h2>
      </header>

      <div class="paper-table paper-header">
        <div class="paper-id">
          <h6>ID</h6>
        </div>
        <div class="paper-read">
          <h6>DL</h6>
        </div>
        <div class="paper-title">
          <h6>Title</h6>
        </div>
        <div class="paper-authors">
          <h6>Authors</h6>
        </div>
        <div class="paper-venue">
          <h6>Bibliographic Info</h6>
        </div>
        <div class="paper-notes">
          <h6>Notes</h6>
        </div>
      </div>

    <?php while ( $post_loop->have_posts() ) : $post_loop->the_post(); ?>
          <div class="paper-desktop">
            <?php get_template_part( 'template-parts/content/publication' ); ?>
          </div>
          <div class="paper-mobile">
            <?php get_template_part( 'template-parts/content/publication-mobile' ); ?>
          </div>
    <?php endwhile; ?>

  </div>
  </section>

  <?php wp_reset_query();?>

<?php else : ?>

  <?php // let's not show anything at all here if results are not returned.. ?>

<?php endif; ?>

  <?php
    $publication_terms_args = array(
      'taxonomy'   => 'publication_year',
      'hide_empty' => false,
    );
    $publication_terms = get_terms( $publication_terms_args );
  ?>


  <?php if ( $publication_terms && ! is_wp_error( $publication_terms )  ) : ?>
    <?php foreach ( $publication_terms as $term ) : ?>
      <?php
       $term_slug = $term->slug;
       $term_name = $term->name;
      ?>


      <?php
        $publication_args = array(
          'post_type' => 'publication',
          'posts_per_page' => -1,
          'meta_key' => 'date',
          'orderby' => 'meta_value',
          'order' => 'DESC',
          'tax_query' => array(
            array(
              'taxonomy' => 'publication_year',
              'field'    => 'slug',
              'terms'    => $term_slug,
            ),
          ),
        );

        // Query for Year
        if ( ! empty( $publication_year )) {
          $publication_args = array_merge_recursive( $publication_args, array(
            'tax_query' => array(
              array(
                'taxonomy'  => 'publication_year',
                'field'     => 'slug',
                'terms'     => $publication_year,
              ),
            ),
          ));
        }

        // Query for Type
        if ( ! empty( $publication_type )) {
          $publication_args = array_merge_recursive( $publication_args, array(
            'tax_query' => array(
              array(
                'taxonomy'  => 'publication_type',
                'field'     => 'slug',
                'terms'     => $publication_type,
              ),
            ),
          ));
        }

        $publication_query = new $wp_query( $publication_args );
      ?>


      <?php if ( $publication_query->have_posts() ) : ?>
        <section class="section paper-section">
          <div class="wrap">
            <header>
              <h2><?php echo $term_name; ?></h2>
            </header>

            <div class="paper-table paper-header">
              <div class="paper-id">
                <h6>ID</h6>
              </div>
              <div class="paper-read">
                <h6>DL</h6>
              </div>
              <div class="paper-title">
                <h6>Title</h6>
              </div>
              <div class="paper-authors">
                <h6>Authors</h6>
              </div>
              <div class="paper-venue">
                <h6>Bibliographic Info</h6>
              </div>
              <div class="paper-notes">
                <h6>Notes</h6>
              </div>
            </div>
              <?php while ( $publication_query->have_posts() ) : $publication_query->the_post(); ?>
                <div class="paper-desktop">
                  <?php get_template_part( 'template-parts/content/publication' ); ?>
                </div>
                <div class="paper-mobile">
                  <?php get_template_part( 'template-parts/content/publication-mobile' ); ?>
                </div>
              <?php endwhile; ?>
          </div>
        </section>
        <?php wp_reset_postdata(); ?>

      <?php else : ?>
        <?php // let's not show anything at all here if results are not returned.. ?>
      <?php endif; ?>

    <?php endforeach; ?>

  <?php endif; ?>
  <!-- end office  -->

<?php get_footer(); ?>
