<?php
/**
 * Displays the staff member template
 */

   $date  = get_field( 'date' );
   $author  = get_field( 'author' );
   $paper_code = get_field('paper_code');
   $notes = get_field( 'notes' );

   $terms = get_the_terms( $post->ID, 'publication_type');
   foreach ( $terms as $term ) {
     $termID[] = $term->term_id;
   }

   $type_color = get_field( 'color', 'publication_type_' . $termID[0] );


?>

  <div class="paper-left-col">
    <div class="paper-id">
      <div class="id-box" style="background-color:<?php echo $type_color;?>;">
      </div>
      <span><?php echo $paper_code; ?></span>
    </div>


    <div class="paper-read">
      <h6>DL</h6>
      <?php if ( have_rows( 'material_links' ) ) : ?>
        <?php while ( have_rows( 'material_links' ) ) : the_row();

         $file_name = get_sub_field( 'file_name' );
         $file = get_sub_field( 'file' );

       ?>

        <a
          href="<?php echo esc_url( $file['url'] ); ?>"
          target="<?php echo esc_attr( $file['target'] ); ?>">
            <?php echo $file_name; ?>
        </a>

      <?php endwhile; ?>
      <?php endif; ?>
    </div>
  </div>

  <div class="paper-right-col">
    <div class="paper-title">
      <h6>title</h6>
      <h5>
        <?php the_title(); ?>
      </h5>
    </div>

    <button class="accordion accord-is-open">
      <span></span>
    </button>

  <div class="accord-container">
    <div class="accord-content">
      <div class="paper-author">
        <h6>Authors</h6>
        <ul>
        <?php if ( have_rows( 'author' ) ) : ?>
          <?php while ( have_rows( 'author' ) ) : the_row();

          $name  = get_sub_field( 'name' );
          $name_or_link  = get_sub_field( 'name_or_link' );
          $author_link  = get_sub_field( 'author_link' );

        ?>


        <li>
          <?php
          if( get_sub_field('name_or_link') == 'name' ) { ?>
            <p><?php echo $name; ?></p>

            <?php
          } else { ?>  <a
               href="<?php echo esc_url( $author_link['url'] ); ?>"
               target="<?php echo esc_attr( $author_link['target'] ); ?>">
               <?php echo $author_link['title']; ?>
             </a> <?php }
             ?>
         </li>

          <?php endwhile; ?>
        <?php endif; ?>
        </ul>
      </div>

      <div class="paper-venue">
        <h6>Bibliographic Info</h6>
        <ul>
        <?php if ( have_rows( 'venue' ) ) : ?>
          <?php while ( have_rows( 'venue' ) ) : the_row();

          $name  = get_sub_field( 'name' );
          $name_or_link  = get_sub_field( 'name_or_link' );
          $author_link  = get_sub_field( 'author_link' );

        ?>

        <li>
          <?php
          if( get_sub_field('name_or_link') == 'name' ) { ?>
            <p><?php echo $name; ?></p>

            <?php
          } else { ?>  <a
               href="<?php echo esc_url( $author_link['url'] ); ?>"
               target="<?php echo esc_attr( $author_link['target'] ); ?>">
               <?php echo $author_link['title']; ?>
             </a> <?php }
             ?>
         </li>

          <?php endwhile; ?>
        <?php endif; ?>
        </ul>
      </div>



      <div class="paper-notes">
        <h6>Notes</h6>
        <?php if ( $notes ) : ?>
          <?php echo $notes; ?>
        <?php endif; ?>



        <?php
      	$supercedes = get_field('supercedes');
      	?>
      	<?php if( $supercedes ): ?>
      		<?php foreach( $supercedes as $supercede ): ?>
              Supercedes: <a href="#publication-<?php the_ID(); ?>">
      					<?php echo get_the_title( $supercede->ID ); ?>
              </a>
      		<?php endforeach; ?>
      	<?php endif; ?>

        <?php
          $supercededs = get_field('superceded');
          ?>
          <?php if( $supercededs ): ?>
            <?php foreach( $supercededs as $superceded ): ?>
              Superceded:  <a href="#publication-<?php the_ID(); ?>">
                  <?php echo get_the_title( $superceded->ID ); ?>
                </a>
            <?php endforeach; ?>
          <?php endif; ?>
      </div>
    </div>
  </div>
</div>



<!-- desktop -->
