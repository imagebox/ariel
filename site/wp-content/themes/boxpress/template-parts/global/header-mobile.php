<?php
/**
 * Mobile Header
 *
 * @package boxpress
 */
?>
<header class="site-header--mobile" role="banner">
  <div class="mobile-header-right">
    <button type="button" class="js-toggle-nav menu-button"
      aria-controls="mobile-nav-tray"
      aria-expanded="false">
      <span class="vh"><?php _e('Menu', 'boxpress'); ?></span>
      <svg class="menu-icon" width="48" height="33" focusable="false">
        <use href="#menu-icon"/>
      </svg>
    </button>
  </div>
</header>

<div id="mobile-nav-tray" class="mobile-nav-tray" aria-hidden="true">
  <div class="mobile-nav-header">
    <div class="site-branding">
      <a href="https://www.harvard.edu/" target="_blank" rel="home">
        <span class="vh"><?php bloginfo('name'); ?></span>
        <svg class="site-logo" width="64" height="77" focusable="false">
          <use href="#site-logo"/>
        </svg>
      </a>
    </div>
    <button type="button" class="js-toggle-nav close-button"
      aria-controls="mobile-nav-tray"
      aria-expanded="false">
      <span class="vh"><?php _e('Close', 'boxpress'); ?></span>
      <svg class="menu-icon-svg" width="33" height="33" focusable="false">
        <use href="#close-icon"/>
      </svg>
    </button>
  </div>
  <nav class="navigation--mobile">

    <?php if ( has_nav_menu( 'primary' )) : ?>
      <ul class="mobile-nav mobile-nav--main">
        <?php
          wp_nav_menu( array(
            'theme_location'  => 'primary',
            'items_wrap'      => '%3$s',
            'container'       => false,
            'walker'          => new Aria_Walker_Nav_Menu(),
          ));
        ?>
      </ul>
    <?php endif; ?>

    <?php if ( has_nav_menu( 'secondary' )) : ?>
      <ul class="mobile-nav mobile-nav--utility">
        <?php
          wp_nav_menu( array(
            'theme_location'  => 'secondary',
            'items_wrap'      => '%3$s',
            'container'       => false,
            'walker'          => new Aria_Walker_Nav_Menu(),
          ));
        ?>
      </ul>
    <?php endif; ?>

  </nav>
</div>
