<?php
/**
 * Template footer
 *
 * Contains the closing of the main el and all content after.
 *
 * @package boxpress
 */



?>

</main>

<?php

$organization_type = get_field( 'organization_type', 'option' );

if ( empty( $organization_type )) {
  $organization_type = 'Organization';
}

 ?>
<?php if ( have_rows( 'site_offices', 'option' )) : ?>

  <address class="address-block" itemscope itemtype="https://schema.org/<?php echo $organization_type; ?>">


    <?php while ( have_rows( 'site_offices', 'option' )) : the_row(); ?>
      <?php
        $street_address = get_sub_field('street_address');
        $address_line_2 = get_sub_field('address_line_2');
        $city           = get_sub_field('city');
        $state          = get_sub_field('state');
        $zip            = get_sub_field('zip');
        $country        = get_sub_field('country');
        $phone_number   = get_sub_field('phone_number');
        $fax_number     = get_sub_field('fax_number');
        $email          = get_sub_field('email');

        $full_street_address = $street_address;

        if ( ! empty( $address_line_2 )) {
          $full_street_address = "{$street_address} <br> {$address_line_2}";
        }
      ?>


<footer id="colophon" class="site-footer" role="contentinfo">
    <div class="primary-footer">
      <div class="wrap">
      <div class="l-footer l-footer--5-cols l-footer--gap-large">
      <div class="l-footer-item">
        <div class="site-branding">
          <a href="https://www.harvard.edu/" target="_blank" rel="home">
            <span class="vh"><?php bloginfo('name'); ?></span>
            <svg class="site-logo" width="64" height="77" focusable="false">
              <use href="#site-logo"/>
            </svg>
          </a>
        </div>
      </div>
      <?php if ( ! empty( $phone_number ) || ! empty( $email )) : ?>
        <div class="l-footer-item">
          <?php if ( ! empty( $email )) : ?>
            <h4 class="rectangle">
              Email
            </h4>
            <p>
              <span class="vh"><?php _e( 'Email:', 'boxpress' ); ?></span>
              <a class="email" href="mailto:<?php echo $email; ?>">
                <span itemprop="email"><?php echo $email; ?></span>
              </a>
            </p>
          <?php endif; ?>
        </div>
        <div class="l-footer-item">
          <?php if ( ! empty( $phone_number )) : ?>
            <h4 class="rectangle">
              Phone
            </h4>
            <?php
              // Strip hyphens & parenthesis for tel link
              $tel_formatted = str_replace([ ".", "-", "–", "(", ")", " " ], '', $phone_number );
            ?>
            <p>
              <span class="vh"><?php _e( 'Phone:', 'boxpress' ); ?></span>
              <a href="tel:+1<?php echo $tel_formatted; ?>">
                <span itemprop="telephone"><?php echo $phone_number; ?></span>
              </a>
            </p>
        </div>
        <?php endif; ?>
        <div class="l-footer-item">
          <?php if ( ! empty( $street_address )) : ?>
            <h4 class="rectangle">
              Address
            </h4>
            <p>
              <span itemprop="streetAddress"><?php echo $full_street_address; ?> <br></span><span itemprop="addressLocality"><?php echo $city; ?></span>,<span itemprop="addressRegion"><?php echo $state; ?></span> <span class="zip"><?php echo $zip; ?></span><br>
            </p>
          <?php endif; ?>
        </div>
        <div class="l-footer-item">
          <?php
            $company_name     = get_bloginfo( 'name', 'display' );
            $alt_company_name = get_field( 'alternative_company_name', 'option' );
            $office_number = get_field( 'office_number', 'option' );

          ?>
          <h4 class="rectangle">
            Office
          </h4>
          <p class="address-title h5">
            <span itemprop="name"><?php echo $alt_company_name; ?></span>
            <br><span itemprop="name"><?php echo $office_number; ?></span>
          </p>
        </div>
      </div>
    </div>

        <?php endif; ?>
      <?php endwhile; ?>
    <?php endif; ?>
  </div>

</footer>

<?php wp_footer(); ?>

<?php // Footer Tracking Codes ?>
<?php the_field( 'footer_tracking_codes', 'option' ); ?>

</body>
</html>
