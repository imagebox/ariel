<?php
/**
 * Displays the staff member template
 */

  $date  = get_field( 'date' );
  $author  = get_field( 'author' );
  $paper_code = get_field('paper_code');
  $notes = get_field( 'notes' );


  $terms = get_the_terms( $post->ID, 'publication_type');
  foreach ( $terms as $term ) {
    $termID[] = $term->term_id;
  }

  $type_color = get_field( 'color', 'publication_type_' . $termID[0] );

?>



<div class="paper-table">

  <div class="paper-id">
    <div class="id-box" style="background-color:<?php echo $type_color;?>;">
    </div>
    <span><?php echo $paper_code; ?></span>
  </div>


  <div class="paper-read">
    <?php
    $file = get_field( 'file' );
    $supercededs = get_field('superceded');
    $alternative_link = get_field('alternative_link');
    ?>
    <?php if ( $supercededs ) :  ?>
      <?php foreach( $supercededs as $superceded ): ?>
        <a class="supersede-box supersede-download" href="#publication-<?php echo $superceded->ID; ?>"></a>
      <?php endforeach; ?>

    <?php else : ?>

    <?php if ( $file ) :  ?>
      <a class="download-icon"
        href="<?php echo esc_url( $file['url'] ); ?>"
        target="_blank">
      </a>
      <?php endif; ?>

    <?php endif; ?>

    <?php if ( $alternative_link ) :  ?>
      <a class="alternative-link"
        href="<?php echo esc_url( $alternative_link['url'] ); ?>"
        target="<?php echo esc_attr( $alternative_link['target'] ); ?>">
      </a>
      <?php endif; ?>
  </div>




  <div class="paper-title">
      <?php the_title(); ?>
  </div>
  <div class="paper-author">
    <?php
      $author_name  = get_field( 'author_name' );
    ?>
    <?php echo $author_name; ?>
  </div>

  <div class="paper-venue">
    <?php
      $bib_info  = get_field( 'bib_info' );
      ?>
        <?php echo $bib_info; ?>
  </div>

  <div class="paper-notes">
    <?php if ( $notes ) : ?>
      <?php echo $notes; ?>
    <?php endif; ?>





  <?php
  $supercedes = get_field('supercedes');
   ?>

  	<?php if( $supercedes ): ?>
        <div class="supersede-box">
          Supersedes
      		<?php foreach( $supercedes as $supercede ): ?>
            <a href="#publication-<?php echo $supercede->ID; ?>">
            <span><?php the_field('paper_code', $supercede->ID); ?></span>
            </a><p>and</p>
          <?php endforeach; ?>
        </div>
  	<?php endif; ?>

    <?php
      $supercededs = get_field('superceded');
      ?>

      <?php if( $supercededs ): ?>
          <div class="supersede-box">
            Superseded by
            <?php foreach( $supercededs as $superceded ): ?>
              <a href="#publication-<?php echo $superceded->ID; ?>">
                <?php the_field('paper_code', $superceded->ID); ?>
              </a>
            <?php endforeach; ?>

          </div>
      <?php endif; ?>
  </div>
</div>

<!-- desktop -->
