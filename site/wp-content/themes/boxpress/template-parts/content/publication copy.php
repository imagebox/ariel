<?php
/**
 * Displays the staff member template
 */

  $date  = get_field( 'date' );
  $author  = get_field( 'author' );
  $paper_code = get_field('paper_code');
  $notes = get_field( 'notes' );

  $terms = get_the_terms( $post->ID, 'publication_type');
  foreach ( $terms as $term ) {
    $termID[] = $term->term_id;
  }

  $type_color = get_field( 'color', 'publication_type_' . $termID[0] );

?>



<div class="paper-table">

  <div class="paper-id" id="publication-<?php the_ID();?>">
    <div class="id-box" style="background-color:<?php echo $type_color;?>;">
    </div>
    <span><?php echo $paper_code; ?></span>
  </div>


  <div class="paper-read">
    <?php if ( have_rows( 'material_links' ) ) : ?>
      <?php while ( have_rows( 'material_links' ) ) : the_row();

       $file_name = get_sub_field( 'file_name' );
       $file = get_sub_field( 'file' );

     ?>


            <?php if ( $file ) : ?>

      <a
        href="<?php echo esc_url( $file['url'] ); ?>"
        target="<?php echo esc_attr( $file['target'] ); ?>">
          <?php echo $file_name; ?>
      </a>

    <?php endif; ?>
    <?php endwhile; ?>
    <?php endif; ?>
  </div>

  <div class="paper-title">
      <?php the_title(); ?>
  </div>
  <div class="paper-author">
    <ul>
    <?php if ( have_rows( 'author' ) ) : ?>
      <?php while ( have_rows( 'author' ) ) : the_row();

      $author_name  = get_sub_field( 'author_name' );

    ?>


      <li>

        <?php echo $author_name; ?>

      </li>

      <?php endwhile; ?>
    <?php endif; ?>
    </ul>
  </div>

  <div class="paper-venue">
    <ul>
    <?php if ( have_rows( 'bibliographic_info' ) ) : ?>
      <?php while ( have_rows( 'bibliographic_info' ) ) : the_row();

      $bib_info  = get_sub_field( 'bib_info' );

      ?>

     <li>

        <?php echo $bib_info; ?>

     </li>

    <?php endwhile; ?>
  <?php endif; ?>
</ul>
  </div>

  <div class="paper-notes">
    <?php if ( $notes ) : ?>
      <?php echo $notes; ?>
    <?php endif; ?>

    <?php
  	$supercedes = get_field('supercedes');
  	?>
  	<?php if( $supercedes ): ?>
  		<?php foreach( $supercedes as $supercede ): ?>
          Supersedes: <a href="#publication-<?php the_ID(); ?>">
  			    <?php the_field('paper_code', $supercede->ID); ?>
          </a>
  		<?php endforeach; ?>
  	<?php endif; ?>

    <?php
      $supercededs = get_field('superceded');
      ?>
      <?php if( $supercededs ): ?>
        <?php foreach( $supercededs as $superceded ): ?>
          Superseded by:  <a href="#publication-<?php the_ID(); ?>">
            <?php the_field('paper_code', $superceded->ID); ?>
            </a>
        <?php endforeach; ?>
      <?php endif; ?>

  </div>
</div>

<!-- desktop -->
