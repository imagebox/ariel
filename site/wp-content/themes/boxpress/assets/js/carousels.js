(function ($) {
  'use strict';




  /**
   * Hero Carousel
   * ---
   */

  var $image_slideshow_el = $('.js-carousel');
  var image_slideshow = null;


   if ( $image_slideshow_el.length ) {
     image_slideshow = tns({
       container: '.js-carousel',
       responsive: {
        0 : {
          items: 1,
        },
        480 : {
          items: 1,
        },
        1200: {
          items: 3,
        }
      },
       nav: true,
       mouseDrag: true,
       controls: true,
       // controlsContainer: "#customize-controls",
       // navContainer: "#customize-thumbnails",
       // navAsThumbnails: true,
       gutter: 20,
       controlsText: [
         '<span class="vh">Previous</span><svg class="svg-left-icon" width="55" height="54" focusable="false"><use href="#carousel-arrow-left-icon"/></svg>',
         '<span class="vh">Next</span><svg class="svg-right-icon" width="55" height="54" focusable="false"><use href="#carousel-arrow-right-icon"/></svg>',
       ],
       "autoplay": false,
       "autoplayHoverPause": true,
      "loop": false,
       "autoplayTimeout": 3500,
       "speed": 400,
       prevButton: '#news-previous',
       nextButton: '#news-next'
     });
   }


})(jQuery);
