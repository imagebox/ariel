<?php
/**
 * Template Name: People
 *
 * Page template to display the advanced page builder.
 *
 * @package BoxPress
 */
get_header(); ?>

  <?php require_once('template-parts/banners/banner--page.php'); ?>

  <?php
    $staff_location_terms_args = array(
      'taxonomy'   => 'people_title',
      'hide_empty' => false,
    );
    $staff_location_terms = get_terms( $staff_location_terms_args );
  ?>

    <?php if ( $staff_location_terms && ! is_wp_error( $staff_location_terms )  ) : ?>
      <?php foreach ( $staff_location_terms as $term ) : ?>
        <?php
         $term_slug = $term->slug;
         $term_name = $term->name;
        ?>

        <?php
          $staff_query_args = array(
            'post_type' => 'people',
            'posts_per_page' => -1,
            'tax_query' => array(
              array(
                'taxonomy' => 'people_title',
                'field'    => 'slug',
                'terms'    => $term_slug,
              ),
            ),
          );
          $staff_query = new $wp_query( $staff_query_args );
        ?>

      <?php if ( $staff_query->have_posts() ) : ?>
        <section class="section staff-section">
          <div class="wrap">
            <header>
              <h2><?php echo $term_name; ?></h2>
            </header>

            <div class="people-card-grid">
            <?php while ( $staff_query->have_posts() ) : $staff_query->the_post(); ?>
                 <?php get_template_part( 'template-parts/content/people' ); ?>
            <?php endwhile; ?>
            </div>


          </div>
        </section>
        <?php wp_reset_postdata(); ?>
      <?php endif; ?>

    <?php endforeach; ?>

  <?php endif; ?>
  <!-- end office  -->

<?php get_footer(); ?>
