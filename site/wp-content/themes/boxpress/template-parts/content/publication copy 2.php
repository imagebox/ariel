<?php
/**
 * Displays the staff member template
 */

  $date  = get_field( 'date' );
  $author  = get_field( 'author' );
  $paper_code = get_field('paper_code');
  $notes = get_field( 'notes' );


  $terms = get_the_terms( $post->ID, 'publication_type');
  foreach ( $terms as $term ) {
    $termID[] = $term->term_id;
  }

  $type_color = get_field( 'color', 'publication_type_' . $termID[0] );

?>



<div class="paper-table">

  <div class="paper-id">
    <div class="id-box" style="background-color:<?php echo $type_color;?>;">
    </div>
    <span><?php echo $paper_code; ?></span>
  </div>


  <div class="paper-read">
    <?php
    $file = get_field( 'file' );
    $supercededs = get_field('superceded');
    ?>
    <?php if ( $supercededs ) :  ?>
      <?php foreach( $supercededs as $superceded ): ?>
        <div class="supersede-box supersede-download">
          <a href="#publication-<?php echo $superceded->ID; ?>">
            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
               viewBox="0 0 16.19 18.79" style="enable-background:new 0 0 16.19 18.79;" xml:space="preserve">
            <g id="jruZSA.tif">
              <g>
                <path d="M8.31,2.93c0-0.84,0-1.67,0-2.53c2.54,1.69,5.06,3.37,7.6,5.06c-2.54,1.69-5.05,3.37-7.59,5.06c0-0.85,0-1.67,0-2.49
                  c-1.97-0.02-4.23,1.43-4.8,3.98c-0.53,2.38,0.88,5.5,4.22,6.07c-2.83-0.09-6.19-2.32-6.89-6.17C0.43,9.6,0.97,7.49,2.48,5.68
                  C3.98,3.87,5.95,2.99,8.31,2.93z"/>
              </g>
            </g>
            </svg>
          </a>
        </div>
      <?php endforeach; ?>

    <?php else : ?>

    <?php if ( $file ) :  ?>
      <a class="download-icon"
        href="<?php echo esc_url( $file['url'] ); ?>"
        target="<?php echo esc_attr( $file['target'] ); ?>">
      </a>
      <?php endif; ?>
    <?php endif; ?>
  </div>




  <div class="paper-title">
      <?php the_title(); ?>
  </div>
  <div class="paper-author">
    <ul>
    <?php
      $author_name  = get_field( 'author_name' );
    ?>

      <li>
        <?php echo $author_name; ?>
      </li>

    </ul>
  </div>

  <div class="paper-venue">
    <ul>

    <?php
      $bib_info  = get_field( 'bib_info' );
      ?>

     <li>
        <?php echo $bib_info; ?>
     </li>

</ul>
  </div>

  <div class="paper-notes">
    <?php if ( $notes ) : ?>
      <?php echo $notes; ?>
    <?php endif; ?>





  <?php
  $supercedes = get_field('supercedes');
   ?>

  	<?php if( $supercedes ): ?>
        <div class="supersede-box">
          Supersedes
      		<?php foreach( $supercedes as $supercede ): ?>
            <a href="#publication-<?php echo $supercede->ID; ?>">
            <span><?php the_field('paper_code', $supercede->ID); ?></span>
            </a><p>and</p>
          <?php endforeach; ?>
        </div>
  	<?php endif; ?>

    <?php
      $supercededs = get_field('superceded');
      ?>

      <?php if( $supercededs ): ?>
          <div class="supersede-box">
            Superseded by
            <?php foreach( $supercededs as $superceded ): ?>
              <a href="#publication-<?php echo $superceded->ID; ?>">
                <?php the_field('paper_code', $superceded->ID); ?>
              </a>
            <?php endforeach; ?>

          </div>
      <?php endif; ?>
  </div>
</div>

<!-- desktop -->
