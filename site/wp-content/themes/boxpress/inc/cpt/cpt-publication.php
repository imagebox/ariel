<?php
/**
 * Department Custom Post Type
 *
 * @package BoxPress
 */

function cpt_publication() {

  $label_plural   = 'Publications';
  $label_singular = 'Publication';

  $labels = array(
    'name'                  => _x( "{$label_plural}", 'Post Type General Name', 'boxpress' ),
    'singular_name'         => _x( "{$label_singular}", 'Post Type Singular Name', 'boxpress' ),
    'menu_name'             => __( "{$label_plural}", 'boxpress' ),
    'name_admin_bar'        => __( "{$label_singular}", 'boxpress' ),
    'parent_item_colon'     => __( "Parent {$label_singular}:", 'boxpress' ),
    'all_items'             => __( "All {$label_plural}", 'boxpress' ),
    'add_new_item'          => __( "Add New {$label_singular}", 'boxpress' ),
    'add_new'               => __( "Add New", 'boxpress' ),
    'new_item'              => __( "New {$label_singular}", 'boxpress' ),
    'edit_item'             => __( "Edit {$label_singular}", 'boxpress' ),
    'update_item'           => __( "Update {$label_singular}", 'boxpress' ),
    'view_item'             => __( "View {$label_singular}", 'boxpress' ),
    'search_items'          => __( "Search {$label_singular}", 'boxpress' ),
    'not_found'             => __( "Not found", 'boxpress' ),
    'not_found_in_trash'    => __( "Not found in Trash", 'boxpress' ),
    'items_list'            => __( "{$label_plural} list", 'boxpress' ),
    'items_list_navigation' => __( "{$label_plural} list navigation", 'boxpress' ),
    'filter_items_list'     => __( "Filter {$label_plural} list", 'boxpress' ),
  );

  $args = array(
    'label'                 => __( "{$label_plural}", 'boxpress' ),
    'description'           => __( "{$label_singular} Custom Post Type", 'boxpress' ),
    'labels'                => $labels,
    'supports'              => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields' ),
    'hierarchical'          => false,
    'public'                => false,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => false, // option for no page
    'exclude_from_search'   => true,
    'publicly_queryable'    => false,
    'capability_type'       => 'post',
    'rewrite'               => false,
    'menu_icon'             => 'dashicons-book',
  );

  register_post_type( 'publication', $args );
}
add_action( 'init', 'cpt_publication', 0 );




/**
 *  Specialty Taxonomy
 */

function tax_publication_type() {

  $label_plural   = 'Publication Types';
  $label_singular = 'Publication Type';

  $labels = array(
    'name'                       => _x( "{$label_plural}", 'Taxonomy General Name', 'boxpress' ),
    'singular_name'              => _x( "{$label_singular}", 'Taxonomy Singular Name', 'boxpress' ),
    'menu_name'                  => __( "{$label_plural}", 'boxpress' ),
    'all_items'                  => __( "All {$label_plural}", 'boxpress' ),
    'parent_item'                => __( "Parent {$label_singular}", 'boxpress' ),
    'parent_item_colon'          => __( "Parent {$label_singular}:", 'boxpress' ),
    'new_item_name'              => __( "New {$label_singular} Name", 'boxpress' ),
    'add_new_item'               => __( "Add New {$label_singular}", 'boxpress' ),
    'edit_item'                  => __( "Edit {$label_singular}", 'boxpress' ),
    'update_item'                => __( "Update {$label_singular}", 'boxpress' ),
    'view_item'                  => __( "View {$label_singular}", 'boxpress' ),
    'separate_items_with_commas' => __( "Separate {$label_plural} with commas", 'boxpress' ),
    'add_or_remove_items'        => __( "Add or remove {$label_plural}", 'boxpress' ),
    'choose_from_most_used'      => __( "Choose from the most used", 'boxpress' ),
    'popular_items'              => __( "Popular {$label_plural}", 'boxpress' ),
    'search_items'               => __( "Search {$label_plural}", 'boxpress' ),
    'not_found'                  => __( "Not Found", 'boxpress' ),
    'items_list'                 => __( "{$label_plural} list", 'boxpress' ),
    'items_list_navigation'      => __( "{$label_plural} list navigation", 'boxpress' ),
  );
  $args = array(
    'labels'                     => $labels,
    'hierarchical'               => true,
    'public'                     => false,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => false,
    'publicly_queryable'         => false,
    'query_var'                  => false,
    'rewrite'                    => false,
  );

  // For location sorting
  register_taxonomy( 'publication_type', 'publication', $args );
}
add_action( 'init', 'tax_publication_type' );



function tax_publication_year() {

  $label_plural   = 'Publication Years';
  $label_singular = 'Publication Year';

  $labels = array(
    'name'                       => _x( "{$label_plural}", 'Taxonomy General Name', 'boxpress' ),
    'singular_name'              => _x( "{$label_singular}", 'Taxonomy Singular Name', 'boxpress' ),
    'menu_name'                  => __( "{$label_plural}", 'boxpress' ),
    'all_items'                  => __( "All {$label_plural}", 'boxpress' ),
    'parent_item'                => __( "Parent {$label_singular}", 'boxpress' ),
    'parent_item_colon'          => __( "Parent {$label_singular}:", 'boxpress' ),
    'new_item_name'              => __( "New {$label_singular} Name", 'boxpress' ),
    'add_new_item'               => __( "Add New {$label_singular}", 'boxpress' ),
    'edit_item'                  => __( "Edit {$label_singular}", 'boxpress' ),
    'update_item'                => __( "Update {$label_singular}", 'boxpress' ),
    'view_item'                  => __( "View {$label_singular}", 'boxpress' ),
    'separate_items_with_commas' => __( "Separate {$label_plural} with commas", 'boxpress' ),
    'add_or_remove_items'        => __( "Add or remove {$label_plural}", 'boxpress' ),
    'choose_from_most_used'      => __( "Choose from the most used", 'boxpress' ),
    'popular_items'              => __( "Popular {$label_plural}", 'boxpress' ),
    'search_items'               => __( "Search {$label_plural}", 'boxpress' ),
    'not_found'                  => __( "Not Found", 'boxpress' ),
    'items_list'                 => __( "{$label_plural} list", 'boxpress' ),
    'items_list_navigation'      => __( "{$label_plural} list navigation", 'boxpress' ),
  );
  $args = array(
    'labels'                     => $labels,
    'hierarchical'               => true,
    'public'                     => false,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => false,
    'publicly_queryable'         => false,
    'query_var'                  => false,
    'rewrite'                    => false,
  );

  // For location sorting
  register_taxonomy( 'publication_year', 'publication', $args );
}
add_action( 'init', 'tax_publication_year' );



/**
 * Search Query Vars
 * ---
 * Used in the Document Archive template for search and sort functionality
 */

function boxpress_document_query_vars( $vars ) {
  $vars[] = 'publication_year';
  $vars[] = 'publication_type';
  return $vars;
}
add_filter( 'query_vars', 'boxpress_document_query_vars' );
