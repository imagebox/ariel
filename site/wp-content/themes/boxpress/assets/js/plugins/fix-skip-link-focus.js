(function () {

  /**
   * Skip Link Focus Fix
   * ===
   * Helps with accessibility for keyboard only users.
   * 
   * Fixes a bug in IE <= 11 where using the tab key to
   * use an anchor link breaks the tab flow of the documnet.
   * 
   * Learn more: https://git.io/vWdr2
   */

  var isIe = /(trident|msie)/i.test( navigator.userAgent );

  if ( isIe && document.getElementById && window.addEventListener ) {
    window.addEventListener( 'hashchange', function() {
      var id = location.hash.substring( 1 ),
        element;

      if ( ! ( /^[A-z0-9_-]+$/.test( id ) ) ) {
        return;
      }

      element = document.getElementById( id );

      if ( element ) {
        if ( ! ( /^(?:a|select|input|button|textarea)$/i.test( element.tagName ) ) ) {
          element.tabIndex = -1;
        }

        element.focus();
      }
    }, false );
  }
})();
