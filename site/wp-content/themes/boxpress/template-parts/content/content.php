<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 * 
 * @package boxpress
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class( 'content' ); ?>>
  <header class="entry-header">
    <h1 class="entry-title">
      <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
    </h1>

    <?php if ( get_post_type() === 'post' ) : ?>

      <?php if ( has_post_thumbnail() ) : ?>
        <a href="<?php the_permalink(); ?>" rel="bookmark">
          <?php the_post_thumbnail('home_index_thumb'); ?>
        </a>
      <?php endif; ?>

      <div class="entry-meta">
        <?php boxpress_posted_on(); ?>
      </div>

    <?php endif; ?>
  </header>

    <?php
      the_content( sprintf(
        __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'boxpress' ),
        the_title( '<span class="screen-reader-text">"', '"</span>', false )
      ));
    ?>

  <footer class="entry-footer">
    <a class="text-button" href="<?php the_permalink(); ?>"><?php _e('Read More', 'boxpress'); ?></a>
  </footer>
</article>
