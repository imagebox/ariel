<?php
/**
 * Displays the staff member template
 */

   $people_image  = get_field( 'people_image' );
   $people_copy  = get_field( 'people_copy' );
   $name_link = get_field( 'name_link' );
   $people_headshot = 'headshot_images';
?>

<?php
if( $name_link ) { ?>
  <a class="people-card"
    href="<?php echo esc_url( $name_link['url'] ); ?>"
    target="<?php echo esc_attr( $name_link['target'] ); ?>">


     <div class="box-left">
       <div class="thumbnail-container">
         <div class="staff-image">
           <img draggable="false" aria-hidden="true"
             src="<?php echo esc_url( $people_image['sizes'][ $people_headshot ] ); ?>"
             width="<?php echo esc_attr( $people_image['sizes'][ $people_headshot . '-width'] ); ?>"
             height="<?php echo esc_attr( $people_image['sizes'][ $people_headshot . '-height'] ); ?>"
             alt="<?php echo esc_attr( $people_image['alt'] ); ?>">
         </div>
       </div>
       <div class="slashes-container">
         <svg class="desktop-slashes" preserveAspectRatio="none" width="145px" height="89px" viewBox="0 0 145 89" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
             <!-- Generator: Sketch 58 (84663) - https://sketch.com -->
             <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                 <g id="Group" fill="#454149" fill-rule="nonzero">
                     <polygon id="Line-3-Copy-3" points="60.2951021 1.01370631 22.6173338 75 18.1784668 75 55.81143 1.00719744"></polygon>
                     <polygon id="Line-3-Copy-6" points="128.336044 1.00517751 90.6690826 75 86.1777649 75 123.819087 0.99209293"></polygon>
                     <polygon id="Line-3-Copy" points="14.3055115 1.00517751 0.0365663081 29.0638324 0.00363393879 20.2967672 9.81464638 1.00160492"></polygon>
                     <polygon id="Line-3-Copy-4" points="82.2990702 1.00517751 44.6668854 75 40.1701355 75 77.8125347 1.00442788"></polygon>
                     <polygon id="Line-3-Copy-7" points="147.051191 9.36111447 113.673782 75 109.193863 75 146.813019 1.00517751 147 1.00517751"></polygon>
                     <polygon id="Path" points="37.3062134 1.01378851 7.10952596e-15 74.3662415 7.10542736e-15 65.5388794 32.8084775 1.01378851"></polygon>
                     <polygon id="Line-3-Copy-5" points="105.296478 1.00517751 67.6763 75 63.1841125 75 100.819427 1"></polygon>
                     <polygon id="Line-3-Copy-8" points="147.002388 54.6913888 136.672239 75.00753 132.183444 75 147.002388 45.8750153"></polygon>
                 </g>
             </g>
         </svg>

         <svg class="mobile-slashes" width="190px" height="150px" viewBox="0 0 190 150" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
             <!-- Generator: Sketch 58 (84663) - https://sketch.com -->
             <title>Group</title>
             <desc>Created with Sketch.</desc>
             <defs>
                 <rect id="path-1" x="0" y="0" width="190" height="149.473684"></rect>
             </defs>
             <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                 <g id="Group">
                     <mask id="mask-2" fill="white">
                         <use xlink:href="#path-1"></use>
                     </mask>
                     <g id="Mask"></g>
                     <g mask="url(#mask-2)" stroke="#454149" stroke-width="4">
                         <g transform="translate(-73.684211, -8.947368)">
                             <path d="M153.157895,0 L70.5263158,161.052632" id="Line-3"></path>
                             <path d="M176.842105,0 L94.2105263,161.052632" id="Line-3-Copy"></path>
                             <path d="M200,0 L117.368421,161.052632" id="Line-3-Copy-2"></path>
                             <path d="M223.684211,0 L141.052632,161.052632" id="Line-3-Copy-3"></path>
                             <path d="M246.842105,0 L164.210526,161.052632" id="Line-3-Copy-4"></path>
                             <path d="M270.526316,0 L187.894737,161.052632" id="Line-3-Copy-5"></path>
                             <path d="M294.210526,0 L211.578947,161.052632" id="Line-3-Copy-8"></path>
                             <path d="M317.368421,0 L234.736842,161.052632" id="Line-3-Copy-9"></path>
                             <path d="M341.052632,0 L258.421053,161.052632" id="Line-3-Copy-10"></path>
                             <path d="M129.473684,0 L46.8421053,161.052632" id="Line-3-Copy-6"></path>
                             <path d="M106.315789,0 L23.6842105,161.052632" id="Line-3-Copy-7"></path>
                             <path d="M82.6315789,0 L0,161.052632" id="Line-3-Copy-11"></path>
                         </g>
                     </g>
                 </g>
             </g>
         </svg>

       </div>
     </div>

    <div class="box-right">
      <div class="body">
        <h3>
          <?php the_title(); ?>
        </h3>
        <?php echo $people_copy; ?>
      </div>
    </div>
  </a>

<?php
} else { ?>

  <div class="people-card">
   <div class="box-left">
     <div class="thumbnail-container">
       <div class="staff-image">
         <img draggable="false" aria-hidden="true"
           src="<?php echo esc_url( $people_image['sizes'][ $people_headshot ] ); ?>"
           width="<?php echo esc_attr( $people_image['sizes'][ $people_headshot . '-width'] ); ?>"
           height="<?php echo esc_attr( $people_image['sizes'][ $people_headshot . '-height'] ); ?>"
           alt="<?php echo esc_attr( $people_image['alt'] ); ?>">
       </div>
     </div>
     <div class="slashes-container">
       <svg class="desktop-slashes" preserveAspectRatio="none" width="145px" height="89px" viewBox="0 0 145 89" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
           <!-- Generator: Sketch 58 (84663) - https://sketch.com -->
           <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
               <g id="Group" fill="#454149" fill-rule="nonzero">
                   <polygon id="Line-3-Copy-3" points="60.2951021 1.01370631 22.6173338 75 18.1784668 75 55.81143 1.00719744"></polygon>
                   <polygon id="Line-3-Copy-6" points="128.336044 1.00517751 90.6690826 75 86.1777649 75 123.819087 0.99209293"></polygon>
                   <polygon id="Line-3-Copy" points="14.3055115 1.00517751 0.0365663081 29.0638324 0.00363393879 20.2967672 9.81464638 1.00160492"></polygon>
                   <polygon id="Line-3-Copy-4" points="82.2990702 1.00517751 44.6668854 75 40.1701355 75 77.8125347 1.00442788"></polygon>
                   <polygon id="Line-3-Copy-7" points="147.051191 9.36111447 113.673782 75 109.193863 75 146.813019 1.00517751 147 1.00517751"></polygon>
                   <polygon id="Path" points="37.3062134 1.01378851 7.10952596e-15 74.3662415 7.10542736e-15 65.5388794 32.8084775 1.01378851"></polygon>
                   <polygon id="Line-3-Copy-5" points="105.296478 1.00517751 67.6763 75 63.1841125 75 100.819427 1"></polygon>
                   <polygon id="Line-3-Copy-8" points="147.002388 54.6913888 136.672239 75.00753 132.183444 75 147.002388 45.8750153"></polygon>
               </g>
           </g>
       </svg>

       <svg class="mobile-slashes" width="190px" height="150px" viewBox="0 0 190 150" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
           <!-- Generator: Sketch 58 (84663) - https://sketch.com -->
           <title>Group</title>
           <desc>Created with Sketch.</desc>
           <defs>
               <rect id="path-1" x="0" y="0" width="190" height="149.473684"></rect>
           </defs>
           <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
               <g id="Group">
                   <mask id="mask-2" fill="white">
                       <use xlink:href="#path-1"></use>
                   </mask>
                   <g id="Mask"></g>
                   <g mask="url(#mask-2)" stroke="#454149" stroke-width="4">
                       <g transform="translate(-73.684211, -8.947368)">
                           <path d="M153.157895,0 L70.5263158,161.052632" id="Line-3"></path>
                           <path d="M176.842105,0 L94.2105263,161.052632" id="Line-3-Copy"></path>
                           <path d="M200,0 L117.368421,161.052632" id="Line-3-Copy-2"></path>
                           <path d="M223.684211,0 L141.052632,161.052632" id="Line-3-Copy-3"></path>
                           <path d="M246.842105,0 L164.210526,161.052632" id="Line-3-Copy-4"></path>
                           <path d="M270.526316,0 L187.894737,161.052632" id="Line-3-Copy-5"></path>
                           <path d="M294.210526,0 L211.578947,161.052632" id="Line-3-Copy-8"></path>
                           <path d="M317.368421,0 L234.736842,161.052632" id="Line-3-Copy-9"></path>
                           <path d="M341.052632,0 L258.421053,161.052632" id="Line-3-Copy-10"></path>
                           <path d="M129.473684,0 L46.8421053,161.052632" id="Line-3-Copy-6"></path>
                           <path d="M106.315789,0 L23.6842105,161.052632" id="Line-3-Copy-7"></path>
                           <path d="M82.6315789,0 L0,161.052632" id="Line-3-Copy-11"></path>
                       </g>
                   </g>
               </g>
           </g>
       </svg>

     </div>
   </div>

  <div class="box-right">
    <div class="body">
      <h3>
        <?php the_title(); ?>
      </h3>
      <?php echo $people_copy; ?>
    </div>
  </div>
  </div>

<?php }
?>
